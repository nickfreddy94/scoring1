var a = [2, 2, 3, 4, 4, 2, 3, 5];
var counts = {},
  i,
  value;
for (i = 0; i < a.length; i++) {
  value = a[i];
  if (typeof counts[value] === "undefined") {
    counts[value] = 1;
  } else {
    counts[value]++;
  }
}
console.log(counts);
